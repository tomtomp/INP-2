-- cpu.vhd: Simple 8-bit CPU (BrainFuck interpreter)
-- Copyright (C) 2015 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): xpolas34
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(12 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (1) / zapis (0)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

 -- zde dopiste potrebne deklarace signalu
 
	type fsm_state is (sidle, sfetch, sdecode, 
							 si_incptr, si_decptr, 
							 si_incval0, si_incval1, si_decval0, si_decval1, 
							 si_print0, si_print1, 
							 si_read0, si_read1, si_read2, 
							 si_ld0, si_ld1, 
							 si_st, 
							 si_null, 
							 si_while0, si_while1, si_while2, si_while3, 
							 si_whilend0, si_whilend1, si_whilend2, si_whilend3);
	signal pstate : fsm_state;
	signal nstate : fsm_state;

	signal pc_reg : std_logic_vector(12 downto 0);
	signal pc_inc : std_logic;
	signal pc_dec : std_logic;
	signal tmp_reg : std_logic_vector(7 downto 0);
	signal tmp_ld : std_logic;
	signal cnt_reg : std_logic_vector(7 downto 0);
	signal cnt_inc : std_logic;
	signal cnt_dec : std_logic;
	signal ptr_reg : std_logic_vector(12 downto 0);
	signal ptr_inc : std_logic;
	signal ptr_dec : std_logic;
	
	signal mx1_sel : std_logic;
	signal mx2_sel : std_logic_vector (1 downto 0);

begin

 -- zde dopiste vlastni VHDL kod
 
--< FSM logic >--
	fsm_pstate: process (RESET, CLK, EN)
	begin
		if (RESET='1') then
			pstate <= sidle;
		elsif rising_edge(CLK) and EN = '1' then
			pstate <= nstate;
		end if;
	end process;
	
	fsm_nstate: process (CLK, RESET, EN)
	begin
		IN_REQ <= '0';
		OUT_WE <= '0';
		OUT_DATA <= "00000000";
		
		mx1_sel <= '0';
		mx2_sel <= "00";
		
		cnt_inc <= '0';
		cnt_dec <= '0';
		tmp_ld <= '0';
		pc_inc <= '0';
		pc_dec <= '0';
		ptr_inc <= '0';
		ptr_dec <= '0';
		
		DATA_EN <= '0';
		DATA_RDWR <= '1';
		
		case pstate is
			when sidle =>
				nstate <= sfetch;
			when sfetch =>
				nstate <= sdecode;
				mx1_sel <= '0';
				DATA_EN <= '1';
				DATA_RDWR <= '1';
			when sdecode =>
				case DATA_RDATA is
					when x"3E" =>
						nstate <= si_incptr;
					when x"3C" =>
						nstate <= si_decptr;
					when x"2B" =>
						nstate <= si_incval0;
					when x"2D" =>
						nstate <= si_decval0;
					when x"2E" =>
						nstate <= si_print0;
					when x"2C" =>
						nstate <= si_read0;
					when x"24" =>
						nstate <= si_ld0;
					when x"21" =>
						nstate <= si_st;
					when x"00" =>
						nstate <= si_null;
					when x"5B" =>
						nstate <= si_while0;
					when x"5D" =>
						nstate <= si_whilend0;
					when others =>
						nstate <= sfetch;
						pc_inc <= '1';
				end case;
				
			when si_incptr =>
				nstate <= sfetch;
				ptr_inc <= '1';
				pc_inc <= '1';
				
			when si_decptr =>
				nstate <= sfetch;
				ptr_dec <= '1';
				pc_inc <= '1';
				
			when si_incval0 =>
				nstate <= si_incval1;
				mx1_sel <= '1';
				DATA_EN <= '1';
				DATA_RDWR <= '1';
			when si_incval1 =>
				nstate <= sfetch;
				pc_inc <= '1';
				mx1_sel <= '1';
				mx2_sel <= "11";
				DATA_EN <= '1';
				DATA_RDWR <= '0';
				
			when si_decval0 =>
				nstate <= si_decval1;
				mx1_sel <= '1';
				DATA_EN <= '1';
				DATA_RDWR <= '1';
			when si_decval1 =>
				nstate <= sfetch;
				mx1_sel <= '1';
				mx2_sel <= "10";
				DATA_EN <= '1';
				DATA_RDWR <= '0';
				pc_inc <= '1';
				
			when si_print0 =>
				if (OUT_BUSY = '1') then
					nstate <= si_print0;
				else
					nstate <= si_print1;
					mx1_sel <= '1';
					DATA_RDWR <= '1';
					DATA_EN <= '1';
				end if;
			when si_print1 =>
				OUT_DATA <= DATA_RDATA;
				--OUT_DATA <= "01100001";
				OUT_WE <= '1';
				nstate <= sfetch;
				pc_inc <= '1';
				
			when si_read0 =>
				IN_REQ <= '1';
				nstate <= si_read1;
			when si_read1 =>
				IN_REQ <= '1';
				if (IN_VLD = '1') then
					nstate <= si_read2;
				else
					nstate <= si_read1;
				end if;
			when si_read2 =>
				IN_REQ <= '1';
				pc_inc <= '1';
				nstate <= sfetch;
				mx1_sel <= '1';
				mx2_sel <= "00";
				DATA_RDWR <= '0';
				DATA_EN <= '1';
				
			when si_ld0 =>
				nstate <= si_ld1;
				mx1_sel <= '1';
				DATA_RDWR <= '1';
				DATA_EN <= '1';
			when si_ld1 =>
				nstate <= sfetch;
				pc_inc <= '1';
				tmp_ld <= '1';
				
			when si_st =>
				nstate <= sfetch;
				pc_inc <= '1';
				mx1_sel <= '1';
				mx2_sel <= "01";
				DATA_RDWR <= '0';
				DATA_EN <= '1';
			
			when si_null =>
				nstate <= sfetch;
				
			when si_while0 =>
				pc_inc <= '1';
				nstate <= si_while1;
				mx1_sel <= '1';
				DATA_RDWR <= '1';
				DATA_EN <= '1';
			when si_while1 =>
				if (DATA_RDATA = "00000000") then
					cnt_inc <= '1';
					nstate <= si_while2;
				else
					nstate <= sfetch;
				end if;
			when si_while2 =>
				if (cnt_reg /= "00000000") then
					nstate <= si_while3;
					mx1_sel <= '0';
					DATA_RDWR <= '1';
					DATA_EN <= '1';
				else
					nstate <= sfetch;
				end if;
			when si_while3 =>
				if (DATA_RDATA = x"5B") then
					cnt_inc <= '1';
					pc_inc <= '1';
					nstate <= si_while2;
				elsif (DATA_RDATA = x"5D") then
					cnt_dec <= '1';
					if (cnt_reg = "00000001") then
						pc_inc <= '1';
						nstate <= sfetch;
					else
						pc_inc <= '1';
						nstate <= si_while2;
					end if;
				else
					pc_inc <= '1';
					nstate <= si_while2;
				end if;
				
			when si_whilend0 =>
				nstate <= si_whilend1;
				mx1_sel <= '1';
				DATA_RDWR <= '1';
				DATA_EN <= '1';
				
			when si_whilend1 =>
				if (DATA_RDATA = "00000000") then
					pc_inc <= '1';
					nstate <= sfetch;
				else
					cnt_inc <= '1';
					pc_dec <= '1';
					nstate <= si_whilend2;
				end if;
				
			when si_whilend2 =>
				if (cnt_reg /= "00000000") then
					nstate <= si_whilend3;
					mx1_sel <= '0';
					DATA_RDWR <= '1';
					DATA_EN <= '1';
				else
					nstate <= sfetch;
				end if;
			
			when si_whilend3 =>
				if (DATA_RDATA = x"5B") then
					cnt_dec <= '1';
					if (cnt_reg = "00000001") then
						pc_inc <= '1';
						nstate <= sfetch;
					else
						pc_dec <= '1';
						nstate <= si_whilend2;
					end if;
				elsif (DATA_RDATA = x"5D") then
					cnt_inc <= '1';
					pc_dec <= '1';
					nstate <= si_whilend2;
				else
					pc_dec <= '1';
					nstate <= si_whilend2;
				end if;
			
		end case;
	end process;

--< Program Counter >--
	cntPC: process (RESET, CLK, EN)
	begin
		if (RESET='1') then
			pc_reg <= (others=>'0');
		elsif rising_edge(CLK) and EN = '1' then
			if (pc_inc='1') then
				if (pc_reg = "0111111111111") then
					pc_reg <= "0000000000000";
				else
					pc_reg <= pc_reg + 1;
				end if;
			elsif (pc_dec='1') then
				if (pc_reg = "0000000000000") then
					pc_reg <= "0111111111111";
				else
					pc_reg <= pc_reg - 1;
				end if;
			end if;
		end if;
	end process;
	
--< TMP Counter >--
	cntTMP: process (RESET, CLK, EN)
	begin
		if (RESET='1') then
			tmp_reg <= (others=>'0');
		elsif rising_edge(CLK) and EN = '1' then
			if (tmp_ld='1') then
				tmp_reg <= DATA_RDATA;
			end if;
		end if;
	end process;
	
--< CNT Counter >--
	cntCNT: process (RESET, CLK, EN)
	begin
		if (RESET='1') then
			cnt_reg <= (others=>'0');
		elsif rising_edge(CLK) and EN = '1' then
			if (cnt_inc='1') then
				cnt_reg <= cnt_reg + 1;
			elsif (cnt_dec='1') then
				cnt_reg <= cnt_reg - 1;
			end if;
		end if;
	end process;	
	
--< PTR Counter >--
	cntPTR: process (RESET, CLK, EN)
	begin
		if (RESET='1') then
			ptr_reg <= "1000000000000";
		elsif rising_edge(CLK) and EN = '1' then
			if (ptr_inc='1') then
				if (ptr_reg = "1111111111111") then
					ptr_reg <= "1000000000000";
				else
					ptr_reg <= ptr_reg + 1;
				end if;
			elsif (ptr_dec='1') then
				if (ptr_reg = "1000000000000") then
					ptr_reg <= "1111111111111";
				else
					ptr_reg <= ptr_reg - 1;
				end if;
			end if;
		end if;
	end process;	

--< MX1 >--
	with mx1_sel select
		DATA_ADDR <= pc_reg when '0', 
						 ptr_reg when '1', 
						 "0000000000000" when others;
						 
--< MX2 >--
	with mx2_sel select
		DATA_WDATA <= IN_DATA when "00", 
						  tmp_reg when "01", 
						  DATA_RDATA - 1 when "10", 
						  DATA_RDATA + 1 when "11", 
						  "00000000" when others;

end behavioral;
 
